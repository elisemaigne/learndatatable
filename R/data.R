#' @title
#' Jeu de données des communes du département 31
#' @description
#' Contient pour chaque commune du département 31, le nom, le code insee, la latitude et la longitude.
#' @format
#' data.table
#' @usage data("communes31")
#'
#' @return
#' A data.table.
#'
#' @examples
#' data("communes31")
#' head(communes31)
"communes31"

#' @title
#' Jeu de données des ventes foncières du département 31 de 2014 à 2023
#'
#' @description
#' Contient pour chaque commune du département 31, les ventes immobilières (extrait du DVF+)
#'
#' @format
#' data.table
#' @usage data("ventes31")
#' @return
#' A data.table.
#' @examples
#' data("ventes31")
#' head(ventes31)
"ventes31"
