
<!-- README.md is generated from README.Rmd. Please edit that file -->

# learnDataTable

<!-- badges: start -->
<!-- badges: end -->

Le but de learnDataTable est de rendre accessibles les tutoriels pour la
formation {data.table} de la plateforme [Genotoul
Biostat](https://genotoul-biostat.pages.mia.inra.fr/website/).

![](https://www.genotoul.fr/wp-content/uploads/2017/01/Biostat_logo-rvb.png)

![](https://raw.githubusercontent.com/Rdatatable/data.table/master/.graphics/logo.png)

## Installation

Il faut d’abord installer le package `LearnDataTable` en exécutant les
commandes ci-dessous :

``` r
library(pak)
pak::pak("git::https://forgemia.inra.fr/elisemaigne/learndatatable.git")
```

## Example

Puis lancer le tutoriel {data.table} qui accompagne la formation :

``` r
library(learnr)
library(learnDataTable)
learnr::run_tutorial("Exercices", "learnDataTable")
```

Dans la console s’affiche une URL qui lance le tutoriel de la formation.
